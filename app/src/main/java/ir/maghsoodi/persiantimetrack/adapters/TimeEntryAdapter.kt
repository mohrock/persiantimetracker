package ir.maghsoodi.persiantimetrack.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import ir.maghsoodi.persiantimetrack.R
import ir.maghsoodi.persiantimetrack.Utils.TimeConversion
import ir.maghsoodi.persiantimetrack.models.report.TimeEntity
import kotlinx.android.synthetic.main.item_time_entry.view.*

class TimeEntryAdapter : RecyclerView.Adapter<TimeEntryAdapter.ArticleViewHolder>() {

    inner class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val differCallback = object : DiffUtil.ItemCallback<TimeEntity>() {
        override fun areItemsTheSame(oldItem: TimeEntity, newItem: TimeEntity): Boolean {
            return oldItem._id == newItem._id
        }

        override fun areContentsTheSame(oldItem: TimeEntity, newItem: TimeEntity): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        return ArticleViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.item_time_entry,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    private var onItemClickListener: ((TimeEntity) -> Unit)? = null

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        val timeEntity = differ.currentList[position]
        holder.itemView.apply {
            tv_duration.text =
                TimeConversion.convertSecondIntToNiceStringWithSecond(timeEntity.timeInterval.duration)
            tv_start.text = TimeConversion.getTimeOfADate(timeEntity.timeInterval.start)
            tv_end.text = TimeConversion.getTimeOfADate(timeEntity.timeInterval.end)

            tv_title.text = timeEntity.description
            tv_project_name.text = timeEntity.projectName
            tv_tags.text = ""
            timeEntity.tags.forEach {
                tv_tags.text = "${tv_tags.text},${it.name}"
            }


        }
    }

    fun setOnItemClickListener(listener: (TimeEntity) -> Unit) {
        onItemClickListener = listener
    }
}