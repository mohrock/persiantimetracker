package ir.maghsoodi.persiantimetrack.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import ir.maghsoodi.persiantimetrack.main.timeEntity.TimeEntityFilter
import ir.maghsoodi.persiantimetrack.main.timeEntity.TimeEntityResult
import ir.maghsoodi.persiantimetrack.Utils.DispatcherProvider
import ir.maghsoodi.persiantimetrack.Utils.Resource
import ir.maghsoodi.persiantimetrack.Utils.TimeConversion
import ir.maghsoodi.persiantimetrack.models.report.TimeEntity
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import timber.log.Timber

class MainViewModel @ViewModelInject constructor(
    private val repository: ReportRepositoryInterface,
    private val dispatchers: DispatcherProvider
) : ViewModel() {

    var todayWeekIndex: Int = 0

    init {
        this.todayWeekIndex =
            TimeConversion.getPersianWeekDayZeroBase(TimeConversion.getCurrentTimeStamp())
    }

    sealed class getReportEventSummary {
        object Success : getReportEventSummary()
        class Failure(val errorText: String) : getReportEventSummary()
        object Loading : getReportEventSummary()
        object Empty : getReportEventSummary()
    }


    private lateinit var timeEntities: List<TimeEntity>

    lateinit var eachDayOfWeekResult: List<String>

    private val _reportEventSummary =
        MutableStateFlow<getReportEventSummary>(getReportEventSummary.Empty)
    val reportEventSummary: StateFlow<getReportEventSummary> = _reportEventSummary

    fun getReportOfTotal() {
        viewModelScope.launch(dispatchers.io) {
            _reportEventSummary.value = getReportEventSummary.Loading
            when (val response = repository.getAllTimeEntity()) {
                is Resource.Error -> _reportEventSummary.value =
                    getReportEventSummary.Failure(response.message!!)
                is Resource.Success -> {
                    val TimeEntities = response.data!!
                    if (TimeEntities == null) {
                        _reportEventSummary.value =
                            getReportEventSummary.Failure("Unexpected error")
                    } else {
                        timeEntities = response.data
                        eachDayOfWeekResult = getSumOfEachDaysOfThisWeek(response.data)
                        _reportEventSummary.value = getReportEventSummary.Success
                    }
                }
            }
        }
    }

    fun getSumOfEachDaysOfThisWeek(totalTimeEntity: List<TimeEntity>): List<String> {
        val output = ArrayList<String>()
        val timeStampOfEachDay =
            TimeConversion.getTimeStampOfEachDayOfWeek(TimeConversion.getCurrentTimeStamp())

        timeStampOfEachDay.forEach {
            val specialDayTimeEntity =
                TimeEntityFilter.filterByDay(dateTime = it, timeEntities = totalTimeEntity)
            val numberOfSecondSpecialDay =
                TimeEntityResult.calculateSumOfTime(specialDayTimeEntity)
            output.add(
                TimeConversion.convertSecondIntToNiceStringWithoutSecond(
                    numberOfSecondSpecialDay
                )
            )
        }
        return output
    }

    fun getTimeEntitiesOfADay(index: Int): List<TimeEntity> {
        val timeStampOfEachDay =
            TimeConversion.getTimeStampOfEachDayOfWeek(TimeConversion.getCurrentTimeStamp())
        return TimeEntityFilter.filterByDay(timeStampOfEachDay[index], timeEntities)
    }
}