package ir.maghsoodi.persiantimetrack.main.timeEntity

import ir.maghsoodi.persiantimetrack.models.report.TimeEntity

class TimeEntityResult {
    companion object {
        fun calculateSumOfTime(timeEntities: List<TimeEntity>): Int {
            var output: Int = 0
            timeEntities.forEach {
                output += it.timeInterval.duration
            }
            return output
        }
    }
}