package ir.maghsoodi.persiantimetrack.main

import ir.maghsoodi.persiantimetrack.Utils.Resource
import ir.maghsoodi.persiantimetrack.Utils.TimeConversion
import ir.maghsoodi.persiantimetrack.api.ClockifyReportApi
import ir.maghsoodi.persiantimetrack.models.report.DetailReportRequest.DetailRequest
import ir.maghsoodi.persiantimetrack.models.report.DetailReportRequest.DetailedFilter
import ir.maghsoodi.persiantimetrack.models.report.TimeEntity
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class ReportRepository @Inject constructor(
    private val api: ClockifyReportApi
) : ReportRepositoryInterface {


    override suspend fun getAllTimeEntity(): Resource<List<TimeEntity>> {
        return try {
            val detailRequest = getRangeOfLast60Day()
            val response =
                api.getDetailedReport(token = "XqBqxp986yaih2bh", detailRequest = detailRequest)
            Timber.d("activity created ${response.code()}")
            val result = response.body()
            if (response.isSuccessful && result != null) {
                Resource.Success(result.timeEntities)
            } else {
                Resource.Error(response.errorBody()!!.string())
            }
        } catch (e: Exception) {
            Resource.Error(e.message ?: "An error occured")
        }
    }

    private fun getRangeOfLast60Day(): DetailRequest {
        var cal = TimeConversion.convertDateToCalender(TimeConversion.getCurrentTimeStamp())
        cal.add(Calendar.DAY_OF_YEAR, -60)
        return DetailRequest(
            TimeConversion.getCurrentTimeStamp(),
            TimeConversion.convertCalenderToDate(cal),
            DetailedFilter(1, 200)
        )
    }


}