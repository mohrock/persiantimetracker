package ir.maghsoodi.persiantimetrack.main

import ir.maghsoodi.persiantimetrack.Utils.Resource
import ir.maghsoodi.persiantimetrack.models.report.TimeEntity

interface ReportRepositoryInterface {

    suspend fun getAllTimeEntity(): Resource<List<TimeEntity>>
}