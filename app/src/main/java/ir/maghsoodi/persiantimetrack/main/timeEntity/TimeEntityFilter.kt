package ir.maghsoodi.persiantimetrack.main.timeEntity

import ir.maghsoodi.persiantimetrack.Utils.TimeConversion
import ir.maghsoodi.persiantimetrack.models.report.TimeEntity
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class TimeEntityFilter {
    companion object {
        fun filterByDay(dateTime: Date, timeEntities: List<TimeEntity>): ArrayList<TimeEntity> {
            val output: ArrayList<TimeEntity> = ArrayList()
            val startTimeStamp = TimeConversion.getFirstTimeStampOfDay(dateTime)
            val endTimeStamp = TimeConversion.getLastTimeStampOfDay(dateTime)
            timeEntities.forEach {
                if (it.timeInterval.start in startTimeStamp..endTimeStamp)
                    output.add(it)
            }
            return output
        }
    }
}