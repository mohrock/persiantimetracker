package ir.maghsoodi.persiantimetrack.models.report

import java.util.*

data class TimeInterval(
    val duration: Int,
    val end: Date,
    val start: Date
)