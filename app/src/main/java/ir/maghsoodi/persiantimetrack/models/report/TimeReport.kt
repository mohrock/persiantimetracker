package ir.maghsoodi.persiantimetrack.models.report

import com.google.gson.annotations.SerializedName

data class TimeReport(
    @SerializedName("timeentries")
    val timeEntities: List<TimeEntity>,
    val totals: List<Total>
)