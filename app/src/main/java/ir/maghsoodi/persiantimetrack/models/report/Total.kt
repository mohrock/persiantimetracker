package ir.maghsoodi.persiantimetrack.models.report

data class Total(
    val entriesCount: Int,
    val totalBillableTime: Int,
    val totalTime: Int
)