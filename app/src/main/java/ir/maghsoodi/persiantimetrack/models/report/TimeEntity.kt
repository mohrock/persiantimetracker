package ir.maghsoodi.persiantimetrack.models.report

data class TimeEntity(
    val _id: String,
    val billable: Boolean,
    val description: String,
    val projectColor: String,
    val projectId: String,
    val projectName: String,
    val tags: List<Tag>,
    val taskId: String,
    val taskName: String,
    val timeInterval: TimeInterval
)