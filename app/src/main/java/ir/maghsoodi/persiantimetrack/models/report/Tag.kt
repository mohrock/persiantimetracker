package ir.maghsoodi.persiantimetrack.models.report

data class Tag(
    val name: String
)