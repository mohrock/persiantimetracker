package ir.maghsoodi.persiantimetrack.models.report.DetailReportRequest

data class DetailedFilter(
    val page: Int,
    val pageSize: Int
)