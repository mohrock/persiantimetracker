package ir.maghsoodi.persiantimetrack.models.report.DetailReportRequest

import java.util.*

data class DetailRequest(
    val dateRangeEnd: Date,
    val dateRangeStart: Date,
    val detailedFilter: DetailedFilter
)