package ir.maghsoodi.persiantimetrack.Utils

import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class TimeConversion {

    companion object {

        fun getCurrentTimeStamp(): Date {
            return Date()
        }

        fun getFirstTimeStampOfDay(input: Date): Date {
            val cal = convertDateToCalender(input)
            cal.set(Calendar.HOUR_OF_DAY, 0)
            cal.set(Calendar.MINUTE, 0)
            cal.set(Calendar.SECOND, 0)
            return convertCalenderToDate(cal)
        }

        fun getLastTimeStampOfDay(input: Date): Date {
            val firstUnixTimeOfDay = getFirstTimeStampOfDay(input)
            val cal = convertDateToCalender(firstUnixTimeOfDay)
            cal.add(Calendar.HOUR_OF_DAY, 24)
            return convertCalenderToDate(cal)
        }

        fun convertDateToCalender(firstUnixTimeOfDay: Date): Calendar {
            val cal = Calendar.getInstance()
            cal.time = firstUnixTimeOfDay
            return cal
        }

        fun convertCalenderToDate(cal: Calendar): Date {
            return cal.time
        }


        fun convertSecondIntToNiceStringWithoutSecond(numberOfSecond: Int): String {
            var hour = numberOfSecond / 3600
            var minutes = (numberOfSecond / 60) % 60
            return "${String.format("%02d", hour)}:${String.format("%02d", minutes)}"
        }

        fun convertSecondIntToNiceStringWithSecond(numberOfSecond: Int): String {
            var second = numberOfSecond % 60
            return "${convertSecondIntToNiceStringWithoutSecond(numberOfSecond)}:${String.format(
                "%02d",
                second
            )}"
        }

        fun getPersianWeekDayZeroBase(timeStamp: Date): Int {
            val georgianWeekDay = convertDateToCalender(timeStamp)[Calendar.DAY_OF_WEEK]
            if (georgianWeekDay == 7)
                return 0
            return georgianWeekDay
        }

        fun getTimeStampOfEachDayOfWeek(currentDate: Date): ArrayList<Date> {
            val output = ArrayList<Date>()


            val currentPersianWeekDay = getPersianWeekDayZeroBase(currentDate)
            repeat(7) {
                var cal = convertDateToCalender(currentDate)
                val distanceDay = it - currentPersianWeekDay
                cal.add(Calendar.DAY_OF_YEAR, distanceDay)
                output.add(convertCalenderToDate(cal))
            }

            return output
        }

        fun getTimeOfADate(timeStamp: Date): String {
            val cal = convertDateToCalender(timeStamp)
            return "${cal.get(Calendar.HOUR_OF_DAY)}:${cal.get(Calendar.MINUTE)}:${cal.get(Calendar.SECOND)}"
        }
    }

}