package ir.maghsoodi.persiantimetrack.di

import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import ir.maghsoodi.persiantimetrack.Utils.Constants
import ir.maghsoodi.persiantimetrack.Utils.DispatcherProvider
import ir.maghsoodi.persiantimetrack.api.ClockifyReportApi
import ir.maghsoodi.persiantimetrack.main.ReportRepository
import ir.maghsoodi.persiantimetrack.main.ReportRepositoryInterface
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {

    @Singleton
    @Provides
    fun provideCurrencyApi(): ClockifyReportApi = Retrofit.Builder()
        .baseUrl(Constants.BASE_URL_Report)
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()))
        .build()
        .create(ClockifyReportApi::class.java)

    @Singleton
    @Provides
    fun provideReportRepositoryInterface(api: ClockifyReportApi): ReportRepositoryInterface = ReportRepository(api)


    @Singleton
    @Provides
    fun provideDispatchers(): DispatcherProvider = object : DispatcherProvider {
        override val main: CoroutineDispatcher
            get() = Dispatchers.Main
        override val io: CoroutineDispatcher
            get() = Dispatchers.IO
        override val default: CoroutineDispatcher
            get() = Dispatchers.Default
        override val unconfined: CoroutineDispatcher
            get() = Dispatchers.Unconfined
    }

}