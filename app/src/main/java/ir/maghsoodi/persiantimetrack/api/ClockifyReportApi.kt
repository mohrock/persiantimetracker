package ir.maghsoodi.persiantimetrack.api

import ir.maghsoodi.persiantimetrack.models.report.DetailReportRequest.DetailRequest
import ir.maghsoodi.persiantimetrack.models.report.TimeReport
import retrofit2.Response
import retrofit2.http.*

interface ClockifyReportApi {
    @Headers("Accept: application/json")
    @POST("/workspaces/{workspaceId}/reports/detailed")
    suspend fun getDetailedReport(
        @Header("X-Api-Key")
        token: String,
        @Path(value = "workspaceId", encoded = true)
        workspaceId: String = "5dc3be54be6e746765c29a2d",
        @Body
        detailRequest: DetailRequest
    ): Response<TimeReport>
}