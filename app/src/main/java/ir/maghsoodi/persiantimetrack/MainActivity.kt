package ir.maghsoodi.persiantimetrack

import android.R
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.hilt.android.AndroidEntryPoint
import ir.maghsoodi.persiantimetrack.adapters.TimeEntryAdapter
import ir.maghsoodi.persiantimetrack.main.MainViewModel
import ir.maghsoodi.persiantimetrack.databinding.ActivityMainBinding
import ir.maghsoodi.persiantimetrack.models.report.TimeEntity
import kotlinx.coroutines.flow.collect
import timber.log.Timber

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    private lateinit var tvOfWeekDaysSummary: Array<TextView>
    private lateinit var llOfWeekDaysCard: Array<LinearLayout>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Timber.d("activity created")
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)


        initialTextViewOfSummary()
        initialLinearLayoutOfWeekDaysCard()

        updateReport()
    }

    private fun initialTextViewOfSummary() {
        tvOfWeekDaysSummary = arrayOf(
            binding.tvSaturdayDuration,
            binding.tvSundayDuration,
            binding.tvMondayDuration,
            binding.tvTuesdayDuration,
            binding.tvWednesdayDuration,
            binding.tvThursdayDuration,
            binding.tvFridayDuration
        )
    }

    private fun initialLinearLayoutOfWeekDaysCard() {
        llOfWeekDaysCard = arrayOf(
            binding.llSaturday,
            binding.llSunday,
            binding.llMonday,
            binding.llTuesday,
            binding.llWednesday,
            binding.llThursday,
            binding.llFriday
        )

        repeat(7) { index ->
            llOfWeekDaysCard[index].setOnClickListener {
                updateViewsForSpecialDay(index)
            }
        }
    }

    private fun updateReport() {
        viewModel.getReportOfTotal()

        lifecycleScope.launchWhenStarted {
            viewModel.reportEventSummary.collect { event ->
                when (event) {
                    is MainViewModel.getReportEventSummary.Success -> {
                        updateReportSummaryForEachDay(viewModel.eachDayOfWeekResult)
                        updateViewsForSpecialDay(viewModel.todayWeekIndex)
                    }
                    is MainViewModel.getReportEventSummary.Failure -> {
                        Timber.d("activity ceatedddddddddddd ${event.errorText}")
                    }

                    else -> Unit
                }
            }
        }
    }

    private fun updateViewsForSpecialDay(index: Int) {
        updateRecyclerViewForSpecialDay(index)
        updateLinearLayoutForSpecialDay(index)
    }

    private fun updateLinearLayoutForSpecialDay(index: Int) {
        repeat(7) {
            llOfWeekDaysCard[it].setBackgroundColor(Color.TRANSPARENT)
        }
        llOfWeekDaysCard[index].setBackgroundColor(Color.parseColor("#392A99"))
    }

    private fun updateRecyclerViewForSpecialDay(index: Int) {
        setupRecyclerView(viewModel.getTimeEntitiesOfADay(index))
    }

    private fun updateReportSummaryForEachDay(eachDayOfWeekResult: List<String>) {
        repeat(7) {
            tvOfWeekDaysSummary[it].text = eachDayOfWeekResult.get(it)
        }
    }

    private fun setupRecyclerView(input: List<TimeEntity>) {
        val timeEntryAdapter = TimeEntryAdapter()
        binding.rvTimes.apply {
            adapter = timeEntryAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
            timeEntryAdapter.differ.submitList(input.toList())
        }
    }

}