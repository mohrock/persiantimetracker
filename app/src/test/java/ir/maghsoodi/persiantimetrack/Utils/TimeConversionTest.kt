package ir.maghsoodi.persiantimetrack.Utils

import junit.framework.TestCase

class TimeConversionTest : TestCase() {

    fun testGetNumberOfMiliSecondInOneDay() {
        assertEquals("this i wrong", 86400000L, TimeConversion.numberOfMiliSecondInOneDay)
    }

    fun testConvertUnixTimeToIso8601() {
        assertEquals(
            "this i wrong",
            "2020-05-10T04:30:00+03:30",
            TimeConversion.convertUnixTimeToIso(1589068800000L)
        )

    }



    fun testConvertIso8601ToUnixTime() {
        assertEquals(
            "this i wrong",
            1589068800000L,
            TimeConversion.convertIsoToUnixTime("2020-05-10T04:30:00+04:30")
        )
    }

    fun testGetCurrentUnixTime() {

    }

    fun testGetUnixTimeOfWeekDays() {
    }

    fun testGetCurrentIsoTime() {

    }

    fun testGetFirstIsoTimeOfToday() {
        assertEquals(
            "this i wrong",
            "2021-01-22T04:30:00+04:30",
            TimeConversion.getFirstIsoTimeOfDay(TimeConversion.getCurrentUnixTime())
        )
    }

    fun testGetLastIsoTimeOfToday() {}

    fun testGetFirstIsoTimeOfWeek() {}

    fun testGetLastIsoTimeOfWeek() {}

    fun testGetFirstIsoTimeOfMonth() {}

    fun testGetLastIsoTimeOfMonth() {}
}